<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.qst.chapter10.javabean.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:forEach items="${applicationScope.onlineRegister}" var="mapRegister">
		<p>
			用户名：${mapRegister.value.username}，会话创建时间：
			<fmt:formatDate value="${mapRegister.value.creationDate}"
				pattern="yyyy-MM-dd HH:mm:ss" />
		</p>
	</c:forEach>
	<a href="loginPro.jsp">注册登录</a>
	<a href="logoutPro.jsp">退出</a>
</body>
</html>