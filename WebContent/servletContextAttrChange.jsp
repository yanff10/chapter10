<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>应用域属性改变监听器使用测试</title>
</head>
<body>
	<%
		application.setAttribute("organization", "QST");
		application.setAttribute("organization", "青软实训");
		application.removeAttribute("organization");
	%>
</body>
</html>