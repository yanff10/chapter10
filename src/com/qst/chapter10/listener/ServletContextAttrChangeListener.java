package com.qst.chapter10.listener;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

/**
 * 应用域属性添加、删除和替换的监听器
 *
 */
@WebListener
public class ServletContextAttrChangeListener implements
		ServletContextAttributeListener {

	public ServletContextAttrChangeListener() {
	}

	/**
	 * 应用域属性添加事件触发方法
	 */
	public void attributeAdded(ServletContextAttributeEvent event) {
		// 获取添加的应用域属性名和属性值
		String attrName = event.getName();
		Object attValue = event.getValue();
		StringBuffer sb = new StringBuffer();
		sb.append("增加的应用域属性名为：");
		sb.append(attrName);
		sb.append("值为：");
		sb.append(attValue);
		event.getServletContext().log(sb.toString());
	}

	/**
	 * 应用域属性删除事件触发方法
	 */
	public void attributeRemoved(ServletContextAttributeEvent event) {
		// 获取删除的应用域属性名和属性值
		String attrName = event.getName();
		Object attValue = event.getValue();
		StringBuffer sb = new StringBuffer();
		sb.append("删除的应用域属性名为：");
		sb.append(attrName);
		sb.append("值为：");
		sb.append(attValue);
		event.getServletContext().log(sb.toString());
	}

	/**
	 * 应用域属性替换事件触发方法
	 */
	public void attributeReplaced(ServletContextAttributeEvent event) {
		// 获取被替换的应用域属性名和属性值
		String attrName = event.getName();
		Object attValue = event.getValue();
		StringBuffer sb = new StringBuffer();
		sb.append("被替换的应用域属性名为：");
		sb.append(attrName);
		sb.append("值为：");
		sb.append(attValue);
		event.getServletContext().log(sb.toString());
	}

}
