package com.qst.chapter10.listener;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

/**
 * 同时实现ServletRequestAttributeListener和ServletRequestListener接口的监听器
 * 
 * @author QST
 */
@WebListener
public class RequestOperatorListener implements ServletRequestListener,
		ServletRequestAttributeListener {

	public RequestOperatorListener() {
	}

	/**
	 * 请求结束时触发该方法
	 */
	public void requestDestroyed(ServletRequestEvent sre) {
		// 获取HttpServletRequest对象
		HttpServletRequest request = (HttpServletRequest) sre
				.getServletRequest();
		String requestURI = request.getRequestURI();
		sre.getServletContext().log(requestURI + "请求结束。");
	}

	/**
	 * 请求对象被初始化时触发该方法
	 */
	public void requestInitialized(ServletRequestEvent sre) {
		// 获取HttpServletRequest对象
		HttpServletRequest request = (HttpServletRequest) sre
				.getServletRequest();
		String requestURI = request.getRequestURI();
		sre.getServletContext().log(requestURI + "请求被初始化。");
	}

	/**
	 * 请求域属性被移除时触发该方法
	 */
	public void attributeRemoved(ServletRequestAttributeEvent srae) {
		// 获取
		String attrName = srae.getName();
		Object attValue = srae.getValue();
		StringBuffer sb = new StringBuffer();
		sb.append("删除的请求域属性名为：");
		sb.append(attrName);
		sb.append("，值为：");
		sb.append(attValue);
		srae.getServletContext().log(sb.toString());
	}

	/**
	 * 添加请求域属性时触发该方法
	 */
	public void attributeAdded(ServletRequestAttributeEvent srae) {
		// 获取
		String attrName = srae.getName();
		Object attValue = srae.getValue();
		StringBuffer sb = new StringBuffer();
		sb.append("添加的请求域属性名为：");
		sb.append(attrName);
		sb.append("，值为：");
		sb.append(attValue);
		srae.getServletContext().log(sb.toString());
	}

	/**
	 * 请求域属性值被替换时触发该方法
	 */
	public void attributeReplaced(ServletRequestAttributeEvent srae) {
		// 获取
		String attrName = srae.getName();
		Object attValue = srae.getValue();
		StringBuffer sb = new StringBuffer();
		sb.append("被替换的请求域属性名为：");
		sb.append(attrName);
		sb.append("，值为：");
		sb.append(attValue);
		srae.getServletContext().log(sb.toString());
	}

}
