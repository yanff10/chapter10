package com.qst.chapter10.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

/**
 * 获取请求访问的资源地址、请求用户名称（若未登录名称为“游客”）、请求用户的IP、请求时间
 * 
 * @author QST
 */
@WebListener
public class UserRequestInfoListener implements ServletRequestListener {

	public UserRequestInfoListener() {
	}

	/**
	 * 请求结束时，容器调用此方法
	 */
	public void requestDestroyed(ServletRequestEvent sre) {
	}

	/**
	 * 请求初始化时，容器调用此方法
	 */
	public void requestInitialized(ServletRequestEvent sre) {
		// 获取HttpServletRequest对象
		HttpServletRequest request = (HttpServletRequest) sre
				.getServletRequest();
		// 获取请求用户IP地址
		String userIP = request.getRemoteAddr();
		// 获取请求资源地址
		String requestURI = request.getRequestURI();
		// 获取已登录请求用户名
		String username = (String) request.getSession()
				.getAttribute("username");
		// 若未登录，设请求用户名为“游客”
		username = (username == null) ? "游客" : username;

		StringBuffer sb = new StringBuffer();
		sb.append("本次请求访问信息：");
		sb.append("用户名称：");
		sb.append(username);
		sb.append(";用户IP:");
		sb.append(userIP);
		sb.append(";请求地址：");
		sb.append(requestURI);
		request.getServletContext().log(sb.toString());
	}

}
