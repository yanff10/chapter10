package com.qst.chapter10.listener;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import com.qst.chapter10.javabean.UserSessionInfo;

/**
 * 实现对在线登录用户名称、会话sessionID、登录时间的显示
 * 
 * @author QST
 */
@WebListener
public class OnlineLoginUserViewListener implements HttpSessionAttributeListener {

	public OnlineLoginUserViewListener() {
	}

	/**
	 * 增加session域属性时，容器调用此方法
	 */
	@SuppressWarnings("unchecked")
	public void attributeAdded(HttpSessionBindingEvent event) {
		HttpSession session = event.getSession();
		// 获取表示用户成功登陆后的会话域属性username
		String username = (String) session.getAttribute("username");
		if (username != null) {
			// 将登陆的用户信息封装到一个JavaBean中
			UserSessionInfo userSessionBean = new UserSessionInfo(username,
					session.getId(), new Date(session.getCreationTime()));
			// 获取保存登录用户信息（Map类型）的应用域属性
			Map<String, UserSessionInfo> onlineRegister = (Map<String, UserSessionInfo>) session
					.getServletContext().getAttribute("onlineRegister");
			if (onlineRegister == null) {
				// 若应用域属性不存在，则实例化一个
				onlineRegister = new HashMap<String, UserSessionInfo>();
			}
			// 将登录用户信息保存在Map结构中，key为：sessionID；value为登录用户信息JavaBean
			onlineRegister.put(session.getId(), userSessionBean);
			// 将更新后的登录用户信息（Map类型）保存到应用域属性中
			session.getServletContext().setAttribute("onlineRegister",
					onlineRegister);
		}
	}

	/**
	 * 删除session域属性时，容器调用此方法
	 */
	public void attributeRemoved(HttpSessionBindingEvent event) {
		// 判断删除的session域属性名称是否为表示用户成功登陆的会话域属性
		if ("username".equals(event.getName())) {
			HttpSession session = event.getSession();
			// 获取保存登录用户信息（Map类型）的应用域属性
			Map<String, UserSessionInfo> onlineRegister = (Map<String, UserSessionInfo>) session
					.getServletContext().getAttribute("onlineRegister");
			// 根据sessionID(key值)将用户信息从应用域属性中移除
			onlineRegister.remove(session.getId());
			// 将更新后的登录用户信息（Map类型）保存到应用域属性中
			session.getServletContext().setAttribute("onlineRegister",
					onlineRegister);
		}
	}

	/**
	 * session域属性被替换时，容器调用此方法
	 */
	public void attributeReplaced(HttpSessionBindingEvent event) {
		// TODO Auto-generated method stub
	}

}
