package com.qst.chapter10.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class SetCharacterEncodingFilter implements Filter {

	String encoding;

	public SetCharacterEncodingFilter() {

	}

	public void init(FilterConfig fConfig) throws ServletException {
		// 获取过滤器配置的初始参数
		this.encoding = fConfig.getInitParameter("encoding");
	}

	public void destroy() {
		this.encoding = null;
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		if (encoding == null)
			encoding = "UTF-8";
		// 设置请求的编码
		request.setCharacterEncoding(encoding);
		// 过滤传递
		chain.doFilter(request, response);
	}

}
