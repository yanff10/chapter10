package com.qst.chapter10.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

@WebFilter(urlPatterns = { "/*" }, initParams = { @WebInitParam(name = "param", value = "青软实训") })
public final class ExampleFilter implements Filter {

	private String attribute = null;

	private FilterConfig filterConfig = null;

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		this.filterConfig = fConfig;
		this.attribute = fConfig.getInitParameter("param");
		filterConfig.getServletContext().log(
				"获得初始化参数param的值为：" + this.attribute);
	}

	@Override
	public void destroy() {
		this.attribute = null;
		this.filterConfig = null;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		long startTime = System.currentTimeMillis();
		filterConfig.getServletContext().log(
				new Date(startTime) + "请求经过" + this.getClass().getName()
						+ "过滤器");

		chain.doFilter(request, response);

		long stopTime = System.currentTimeMillis();
		filterConfig.getServletContext().log(
				new Date(stopTime) + "响应经过" + this.getClass().getName()
						+ "过滤器，本次请求响应过程花费 " + (stopTime - startTime) + " 毫秒");

	}

}
