package com.qst.chapter10.filter;

import java.io.*;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.*;

import java.util.zip.GZIPOutputStream;

@WebFilter("/GZIPEncodeFilter")
public class GZIPEncodeFilter implements Filter {

	public void init(FilterConfig filterConfig) {

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpreq = (HttpServletRequest) request;

		if (isGzipEncoding(httpreq)) {
			// 如果请求浏览器支持GZIP，包装一个包含压缩功能的响应对象
			GZIPEncodableResponse wrappedResponse = new GZIPEncodableResponse(
					(HttpServletResponse) response);
			// 通知浏览器输出信息使用GZIP压缩
			((HttpServletResponse) response).setHeader("Content-Encoding",
					"gzip");
			// 将包装的响应对象传递给目标资源
			chain.doFilter(request, wrappedResponse);
			// 刷新并关闭响应输出流，响应经GZIP压缩后的信息
			wrappedResponse.flush();
			
		} else {
			chain.doFilter(request, response);
		}
	}

	public void destroy() {
	}

	/**
	 * 判断浏览器是否支持GZIP
	 * 
	 * @param request
	 * @return
	 */
	private static boolean isGzipEncoding(HttpServletRequest request) {
		boolean flag = false;
		// 获取请求浏览器支持的编码方式
		String encoding = request.getHeader("Accept-Encoding");
		if (encoding != null && encoding.toLowerCase().indexOf("gzip") != -1) {
			flag = true;
		}
		return flag;
	}

	/**
	 * 自定义的包含压缩功能的响应对象包装类
	 * 
	 * @author QST
	 */
	private class GZIPEncodableResponse extends HttpServletResponseWrapper {
		// 经过压缩包装后的输出流对象
		private GZIPServletStream wrappedOut;

		public GZIPEncodableResponse(HttpServletResponse response)
				throws IOException {
			super(response);
			// 对响应输出流进行压缩处理
			wrappedOut = new GZIPServletStream(response.getOutputStream());
		}

		public ServletOutputStream getOutputStream() throws IOException {
			return wrappedOut;
		}

		private PrintWriter wrappedWriter;

		/**
		 * 将响应信息输出到GZIPServletStream输出流中
		 */
		public PrintWriter getWriter() throws IOException {
			if (wrappedWriter == null) {
				wrappedWriter = new PrintWriter(new OutputStreamWriter(
						getOutputStream(), getCharacterEncoding()));
			}
			return wrappedWriter;
		}

		/**
		 * 刷新PrintWriter输出流的内容，关闭GZIPServletStream响应输出流
		 * 
		 * @throws IOException
		 */
		public void flush() throws IOException {
			if (wrappedWriter != null) {
				wrappedWriter.flush();
			}
			System.out.println("GZIPEncodableResponse flush()........");
			wrappedOut.finish();
		}
	}

	/**
	 * 输出流对象包装类，用于向输出流中写入压缩成GZIP格式的数据
	 * 
	 * @author QST
	 *
	 */
	private class GZIPServletStream extends ServletOutputStream {

		private GZIPOutputStream outputStream;
		byte[] buf;
		public GZIPServletStream(OutputStream source) throws IOException {
			outputStream = new GZIPOutputStream(source);
		}

		public void finish() throws IOException {
			outputStream.finish();
			System.out.println("GZIPServletStream finish()()");
		}

		public void write(byte[] buf) throws IOException {
			outputStream.write(buf);
		}

		public void write(byte[] buf, int off, int len) throws IOException {
			outputStream.write(buf, off, len);
		}

		public void write(int c) throws IOException {
			outputStream.write(c);
		}

		public void flush() throws IOException {
			outputStream.flush();
			System.out.println("GZIPServletStream flush()");
		}

		public void close() throws IOException {
			outputStream.close();
			System.out.println("GZIPServletStream close()");
		}

		@Override
		public boolean isReady() {
			return false;
		}

		@Override
		public void setWriteListener(WriteListener arg0) {

		}
	}
}