package com.qst.chapter10.javabean;

import java.util.Date;

public class UserSessionInfo {

	private String username;
	
	private String sessionID;
	
	private Date creationDate;
	
	public UserSessionInfo(){
		
	}

	public UserSessionInfo(String username, String sessionID, Date creationDate) {
		super();
		this.username = username;
		this.sessionID = sessionID;
		this.creationDate = creationDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

}
