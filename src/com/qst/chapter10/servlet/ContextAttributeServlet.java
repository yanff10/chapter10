package com.qst.chapter10.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ContextAttributeServlet")
public class ContextAttributeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ContextAttributeServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// 设置响应到客户端MIME类型和字符编码方式
		response.setContentType("text/html;charset=UTF-8");
		//获取ServletContext对象
		ServletContext context = super.getServletContext();
		//从ServletContext对象获取count属性存放的计数值
		Integer count = (Integer) context.getAttribute("count");
		if (count == null) {
			count = 1;
		} else {
			count = count + 1;
		}
		//将更新后的数值存放到ServletContext对象的count属性中
		context.setAttribute("count", count);
		//获取输出流
		PrintWriter out = response.getWriter();
		//输出计数信息
		out.println("<p>本网站目前访问人数是： " + count + "</p>");
	}
	
}
